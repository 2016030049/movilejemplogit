package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    // crear la estructura

    private EditText txt1;
    private EditText txt2 ;
    private EditText txtRes;
    private Button btnSumar;
    private  Button  btnRestar;
    private  Button  btnMulti;
    private  Button  btnDiv;
    private  Button  btnLimpiar;
    private  Button  btnCerrar;
    private Operaciones op = new Operaciones();


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();



    }


    public void initComponents(){

        txt1 = (EditText) findViewById(R.id.txtNum1);
        txt2 = (EditText) findViewById(R.id.txtNum2);
        txtRes = (EditText) findViewById(R.id.txtRes);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);





    }

    public void setEventos(){

this.btnSumar.setOnClickListener(this);
this.btnRestar.setOnClickListener(this);
this.btnDiv.setOnClickListener(this);
this.btnMulti.setOnClickListener(this);
this.btnLimpiar.setOnClickListener(this);
this.btnCerrar.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

            switch( view.getId()){

                case  R.id.btnSuma :
                    sumar();
                    break;
                case  R.id.btnResta :
                    resta();
                    break;
                case  R.id.btnMult :
                    multiplicacion();
                    break;
                case  R.id.btnDivi :
                    division();
                    break;
                case  R.id.btnLimpiar :
                    limpiar();
                    break;
                case  R.id.btnCerrar :
                    cerrar();
                    break;

            }

    }

    public void sumar(){
        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.suma()));
    }
    public void resta(){

        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.resta()));

    }
    public void multiplicacion(){

        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.mult()));

    }
    public void division(){

        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.div()));

    }
    public void limpiar(){
        txt1.setText("");
        txt2.setText("");
        txtRes.setText("");

    }
    public void cerrar(){
        finish();

    }


}
